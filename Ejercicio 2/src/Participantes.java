import java.util.HashSet;

public class Participantes {
	private HashSet<Integer> numeroBingo;
	private int Id;
	
	public Participantes(int Id) {
		this.Id = Id;
		numeroBingo = new HashSet<>();
	}
	public HashSet<Integer> getNumeroBingo(){
		return numeroBingo;
	}
	public void setNumeroBingo(int numeroBingo) {
		this.numeroBingo.add(numeroBingo);
	}
	public int getId() {
		return Id;
	}
	public void setId(int Id) {
		this.Id = Id;
	}
}
