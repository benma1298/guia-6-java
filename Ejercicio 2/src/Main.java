
public class Main {

	public static void main(String[] args) {
		Moderador m = new Moderador();
		Participantes p1, p2, p3, p4;
		p1 = new Participantes(1);
		p2 = new Participantes(2);
		p3 = new Participantes(3);
		p4 = new Participantes(4);
		Simulador s1, s2, s3, s4;
		s1 = new Simulador(m.getNumero(), p1);
		s2 = new Simulador(m.getNumero(), p2);
		s3 = new Simulador(m.getNumero(), p3);
		s4 = new Simulador(m.getNumero(), p4);
		s1.numeroAleatorio();
		s2.numeroAleatorio();
		s3.numeroAleatorio();
		s4.numeroAleatorio();
		s1.decirNumero();
		s2.decirNumero();
		s3.decirNumero();
		s4.decirNumero();
		int bingo = 0;
		int bingo_p1;
		int bingo_p2;
		int bingo_p3;
		int bingo_p4;
		while(bingo == 0) {
			s1.definirNumero();
			s2.definirNumero();
			s3.definirNumero();
			s4.definirNumero();
			bingo_p1 = s1.ganar();
			bingo_p2 = s2.ganar();
			bingo_p3 = s3.ganar();
			bingo_p4 = s4.ganar();
			if(bingo_p1 == 1 || bingo_p2 == 1 || bingo_p3 == 1 || bingo_p4 == 1) {
				bingo = 1;	
			}
		}
	}
}
