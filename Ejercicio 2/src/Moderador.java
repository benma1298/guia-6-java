import java.util.HashSet;

public class Moderador {
	private HashSet<Integer> numero;
	
	public Moderador() {
		numero = new HashSet<>();
	}
	public HashSet<Integer> getNumero(){
		return numero;
	}
	public void setNumero(int nAleatorio) {
		this.numero.add(nAleatorio);
	}
}
