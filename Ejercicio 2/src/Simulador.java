import java.util.HashSet;
import java.util.Iterator;

public class Simulador implements Controlador {
	HashSet<Integer> numero;
	Participantes p;
	
	public Simulador(HashSet<Integer> n, Participantes p) {
		this.numero = n;
		this.p = p;
	}
	public void numeroAleatorio() {
		for(int i=0; i<6; i++) {
			int cont = 0;
			if(p.getNumeroBingo().size() == 0) {
				int random = (int)(Math.random()*99+1);
				p.setNumeroBingo(random);
			}
			else {
				while(cont == 0) {
					int random = (int)(Math.random()*99+1);
					if(!p.getNumeroBingo().contains(random)) {
						p.setNumeroBingo(random);
						cont++;
					}
				}
			}
		}
	}
	public void decirNumero() {
		System.out.println("El participante: " + p.getId() + ", tiene los siguientes numeros");
		Iterator<Integer> iterate = p.getNumeroBingo().iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
	}
	public void definirNumero() {
		int cont = 0;
		int random = -1;
		if(numero.size() == 0) {
			random = (int)(Math.random()*99+1);
			numero.add(random);
		}
		else {
			while(cont == 0) {
				random = (int)(Math.random()*99+1);
				if(!numero.contains(random)) {
					numero.add(random);
					cont++;
				}
			}
		}
		System.out.println("Numero cantado: " + random);
		System.out.println("Numeros cantados: " + numero);
	}
	
	public int ganar() {
		int ganar = 0;
		if(numero.containsAll(p.getNumeroBingo())) {
			ganar = 1;
			System.out.println(p.getId() + " es el ganador de este Bingo, con los numeros: \n"+ p.getNumeroBingo());
		}
		return ganar;
	}
}
