import java.util.HashSet;
import java.util.Iterator;

public class Sistema {
	Sistema(){
		HashSet<String> estudiantes = new HashSet<>();
		estudiantes.add("Jorge Valenzuela");
		estudiantes.add("Francisca Quezada");
		estudiantes.add("Bairon Escobar");
		estudiantes.add("Tomas Piderit");
		estudiantes.add("Cristian Agurto");
		estudiantes.add("Benjamin Martin");
		estudiantes.add("Valentina Figueroa");
		
		System.out.println("Conjunto de estudiantes: ");
		Iterator<String> iterate = estudiantes.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		
		System.out.println("\n Existe el estudiante Tomas Piderit en Programacion Avanzada?\n" + estudiantes.contains("Tomas Piderit"));
		
		HashSet<String> estudiantes2020 = new HashSet<>();
		estudiantes2020.add("Jaime Martin");
		estudiantes2020.add("Juan Soto");
		estudiantes2020.add("Fernando Ramos");
		estudiantes2020.add("Carolina Salinas");
		estudiantes2020.add("Veronica Albornoz");
		estudiantes2020.add("Pedro Gonzales");
		estudiantes2020.add("Karen Rivera");
		System.out.println("Conjunto de estudiantes 2020: ");
		Iterator<String> iterar = estudiantes2020.iterator();
		while(iterar.hasNext()) {
			System.out.println(iterar.next());
		}
		System.out.println("Existe elemento de estudiantes en estudiantes2020?" + estudiantes2020.containsAll(estudiantes));
		
		estudiantes.addAll(estudiantes2020);
		System.out.println("Se han unido los conjuntos estudiantes y estudiantes2020");
		System.out.println("Nuevo conjunto estudiantes: ");
		System.out.println(estudiantes);
		
		estudiantes2020.retainAll(estudiantes);
		System.out.println(estudiantes2020);
	}

}
